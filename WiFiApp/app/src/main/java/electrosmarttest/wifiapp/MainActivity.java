package electrosmarttest.wifiapp;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    private ArrayAdapter<String> arrayAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ListView listView_wifi = (ListView) findViewById(R.id.listview_wifi_access);
        arrayAdapter = new ArrayAdapter<String>(this,R.layout.listed_wifi_access_point);
        listView_wifi.setAdapter(arrayAdapter);

        final TimerTask updateWifi = new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        updateWifiNetwork();
                    }
                });
            }
        };

        Timer timer = new Timer();
        timer.schedule(updateWifi, 0, 5000);
    }

    public void updateWifiNetwork() {
        HashMap<String,Integer> results_hash = new HashMap<>();

        WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        List<ScanResult> list_results = wifiManager.getScanResults();//added ACCESS_WIFI_STATE

        for(ScanResult s : list_results) {
            if(s.SSID!=null) {
                if(results_hash.get(s.SSID)!=null ) {
                    if(results_hash.get(s.SSID)<s.level)
                        results_hash.put(s.SSID,s.level);
                }
                else {
                    results_hash.put(s.SSID,s.level);
                }
            }
            else {
                results_hash.put("(SSID masqué) "+s.BSSID+" ",s.level);
            }
        }

        List<String> list_name_wifi = new ArrayList<String>();
        list_name_wifi.addAll(results_hash.keySet());

        for(int i=0;i<list_name_wifi.size();i++) {
            list_name_wifi.set(i,list_name_wifi.get(i)+" "+results_hash.get(list_name_wifi.get(i)));
        }
        Log.d("blou",list_name_wifi.toString());
        arrayAdapter.clear();
        arrayAdapter.addAll(list_name_wifi);
        arrayAdapter.notifyDataSetChanged();
    }
}
